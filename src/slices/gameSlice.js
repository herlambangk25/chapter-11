import { createSlice } from '@reduxjs/toolkit';

const gameSlice = createSlice({
  name: 'game',
  initialState: {
    playerChoice: '',
    computerChoice: '',
    result: '',
  },
  reducers: {
    choose: (state, action) => {
      state.playerChoice = action.payload;
      state.computerChoice = getComputerChoice();
      state.result = calculateResult(state.playerChoice, state.computerChoice);
    },
  },
});

export const { choose } = gameSlice.actions;
export default gameSlice.reducer;
