import { configureStore } from "@reduxjs/toolkit";
import counterSlice from "../reducer";
// import { gameReducer } from '../../src/slices/gameSlice';
import { gameReducer } from '../../src/slices/gameSlice';

// const allReducers = combineReducers({
//   counterReducer: counterSlice.reducer})

// const myStore = configureStore({
//   reducer: allReducers
// })

export const store = configureStore({
  reducer: {
    game: gameReducer,
    counterReducer: counterSlice.reducer
  },
});

export default store