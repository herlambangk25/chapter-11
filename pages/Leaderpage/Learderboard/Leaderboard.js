import { Text } from "@nextui-org/react";
import  Box  from "../../Box";
import React from "react";
import { set, ref, push, onValue, child, update, getDatabase, Database } from "firebase/database";
import { authInstance, databaseInstance } from "../../../services/firebase";
import { useEffect, useState } from 'react';

//function untuk mengambil data dari firebase
const getLeaderboardData = () => {
  return new Promise((resolve, reject) => {
      const dbref = ref(databaseInstance, '/scoreDummyGames')
      onValue(dbref, (snapshot) => {
          const value = []
          Object.keys(snapshot.val()).map(key => {
              value.push({
                  id : key,
                  data : snapshot.val()[key]
              })
          })
          resolve(value)
      })
  })
}

export default function LeaderAbout() {
  const [leaderboard, setLeaderboard] = useState([]);
    
  //function untuk memasukan data dari firebase ke state array 'leaderboard'
  const getAllLeaderboardData = async () => {
  const resp = await getLeaderboardData();
  setLeaderboard(resp);
  console.log(leaderboard);
  };

  return (
    <div>
      <Box
        css={{
          textAlign: "center",
          px: "$12",
          mt: "$10",
          "@xsMax": { px: "$10" },
        }}
      >
        <Text
          h2
          css={{
            textAlign: "center",
            px: "$12",
            mt: "50px",
            "@xsMax": { px: "$10" },
          }}
          size={50}
          weight="bold"
        >
          Leaderboard
        </Text>
        <table className="tableLeaderboard">
                <thead>
                    <tr>
                        <th scope="col">Username</th>
                        <th scope="col">Score</th>
                    </tr>
                </thead>
                <tbody>
                    {leaderboard.map((value) => (
                        <tr key={value.id}>
                            <td>{value.data.email}</td>
                            <td>{value.data.score}</td>
                        </tr>
                    ))}
                </tbody>
            </table>
            <button onClick={getAllLeaderboardData}>Data</button>
      </Box>
    </div>
  );
}
