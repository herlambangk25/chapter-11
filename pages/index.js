import Head from "next/head";
import Header from "../pages/Header";
import  Card1  from "./components/Card/Card1";
// import Footer from "./Footer";
import  Layout  from "./Layout";

export default function App() {
  return (
    <>
      <Head>
        <title>Kelompok-1 | FSW-Wave 26 </title>
        <meta name="description" content="Website Kelompok Satu" />
      </Head>
      <Header />
      <Layout />
      <Card1 />
    </>
  );
}
