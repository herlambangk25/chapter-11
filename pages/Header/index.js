import { Navbar, Link, Text, Button } from '@nextui-org/react';
import  AcmeLogo  from '../AcmeLogo.js';

export default function App() {
  const collapseItems = ['About', 'Profile', 'Contact', 'LeaderBoard'];

  return (
    <Navbar isBordered variant="sticky">
      <Navbar.Toggle showIn="xs" />
      <Navbar.Brand
        css={{
          '@xs': {
            w: '12%',
          },
        }}
      >
        <AcmeLogo />
        <Link href="/">
          <Text b color="inherit" hideIn="xs">
            Binar Kelompok-1
          </Text>
        </Link>
      </Navbar.Brand>
      <Navbar.Content
        enableCursorHighlight
        activeColor="warning"
        hideIn="xs"
        variant="highlight"
      >
        <Navbar.Link href="/About">About</Navbar.Link>
        <Navbar.Link href="/Contact">Profile</Navbar.Link>
        <Navbar.Link href="/Profile">Contact</Navbar.Link>
        <Navbar.Link href="/Leaderpage">LeaderBoard</Navbar.Link>
      </Navbar.Content>
      <Navbar.Content
        css={{
          '@xs': {
            w: '12%',
            jc: 'flex-end',
          },
        }}
      >
        <Navbar.Link href="/Login" color="inherit">
          Login
        </Navbar.Link>
        <Navbar.Item>
          <Button auto flat as={Link} href="/Signup">
            Sign Up
          </Button>
        </Navbar.Item>
      </Navbar.Content>
      <Navbar.Collapse disableAnimation>
        {collapseItems.map((item, index) => (
          <Navbar.CollapseItem
            key={item}
            activeColor="warning"
            css={{
              color: index === collapseItems.length - 1 ? '$error' : '',
            }}
            isActive={index === 2}
          >
            <Link
              color="inherit"
              css={{
                minWidth: '100%',
              }}
              href="#"
            >
              {item}
            </Link>
          </Navbar.CollapseItem>
        ))}
      </Navbar.Collapse>
    </Navbar>
  );
}
