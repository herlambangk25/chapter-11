import { FaHandRock, FaHandPaper, FaHandScissors } from 'react-icons/fa';
import { useState } from 'react';
import { choose } from '@/src/slices/gameSlice';
import { useDispatch, useSelector } from 'react-redux';
import Link from 'next/link';

const actions = {
  rock: 'scissors',
  paper: 'rock',
  scissors: 'paper',
};

function randomAction() {
  const keys = Object.keys(actions);
  const index = Math.floor(Math.random() * keys.length);

  return keys[index];
}

function calculateWinner(playerChoice, computerChoice) {
  if (playerChoice === computerChoice) {
    return 0;
  } else if (actions[playerChoice].includes(computerChoice)) {
    return -1;
  } else if (actions[playerChoice].includes(computerChoice)) {
    return 1;
  }

  return null;
}

function ActionIcon({ action, ...props }) {
  const icons = {
    rock: FaHandRock,
    paper: FaHandPaper,
    scissors: FaHandScissors,
  };
  const Icon = icons[action];
  return <Icon {...props} />;
}

function Player({ name = 'Player', score = 0, action = 'rock' }) {
  return (
    <div className="player">
      <div className="score">{`${name}: ${score}`}</div>
      <div className="action">
        {action && <ActionIcon action={action} size={60} />}
      </div>
    </div>
  );
}

function ActionButton({ action = 'rock' }) {
  const dispatch = useDispatch();
  return (
    <button className="round-btn" onClick={() => dispatch(choose(action))}>
      <ActionIcon action={action} size={20} />
    </button>
  );
}

function ShowWinner() {
  const text = {
    '-1': 'You Win!',
    0: "It's a Tie",
    1: 'You Lose!',
  };
  const winner = useSelector((state) => state.text);

  return (
    <div>
      <h2>{text[winner]}</h2>
    </div>
  );
}

export default function RpsGame() {
  const [playerAction, setPlayerAction] = useState('');
  const [computerAction, setComputerAction] = useState('');

  const [playerScore, setPlayerScore] = useState(0);
  const [computerScore, setComputerScore] = useState(0);
  const [winner, setWinner] = useState(0);

  const onActionSelected = (selectedAction) => {
    const newComputerAction = randomAction();

    setPlayerAction(selectedAction);
    setComputerAction(newComputerAction);

    const newWinner = calculateWinner(selectedAction, newComputerAction);
    setWinner(newWinner);
    if (newWinner === -1) {
      setPlayerScore(playerScore + 1);
    } else if (newWinner === 1) {
      setComputerScore(computerScore + 1);
    }
  };

  return (
    <div className="center">
      <div>
        <h1>Lets Play</h1>
        <div className="container">
          <Player name="Player" score={playerScore} action={playerAction} />
          <Player
            name="Computer"
            score={computerScore}
            action={computerAction}
          />
        </div>
        <div>
          <ActionButton action="rock" onActionSelected={onActionSelected} />
          <ActionButton action="paper" onActionSelected={onActionSelected} />
          <ActionButton action="scissors" onActionSelected={onActionSelected} />
        </div>
        <ShowWinner winner={winner} />
      </div>
      <Link href="/">
        <button className="home-button">HOME</button>
      </Link>
    </div>
  );
}
