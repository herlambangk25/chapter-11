import { Text } from "@nextui-org/react";
import Box  from "../../Box";

import React from "react";

export default function Contactpage() {
  return (
    <div>
      <Box
        css={{
          textAlign: "center",
          px: "$12",
          mt: "$10",
          "@xsMax": { px: "$10" },
        }}
      >
        <Text
          h2
          css={{
            textAlign: "center",
            px: "$12",
            mt: "300px",
            "@xsMax": { px: "$10" },
          }}
          size={25}
          weight="bold"
        >
          Halaman ini menampilkan informasi Contact.
        </Text>
      </Box>
    </div>
  );
}
