import { useDispatch, useSelector } from "react-redux";
import { action } from "../../redux/reducer/index";
import { set, ref, push, onValue, child, update, getDatabase, Database } from "firebase/database";
import { authInstance, databaseInstance } from "../../services/firebase";
import Link from "next/link";

export default function DetailPage() {

  const counter = useSelector((state) => {
    return state.counterReducer.counter
  })
  
  const dispatch = useDispatch()
  
  const increment = () =>{
    dispatch(action.increment())
  }
  
  const decrease = () => {
    dispatch(action.decrement())
  }
  
  const increaseTen = () => {
    dispatch(action.increaseByNumber({ number: 10}))
  }

  function insertScore() {
    // A post entry.
    const user = authInstance.currentUser.email;
    const postData = {
      email : user,
      score: counter
    };
  
    // Get a key for a new Post.
    const newPostKey = push(child(ref(databaseInstance), 'posts')).key;
  
    // Write the new post's data simultaneously in the posts list and the user's post list.
    const updates = {};
    updates['/scoreDummyGames' + '/' + newPostKey] = postData;
  
    return update(ref(databaseInstance), updates);
  }

  //auth
//   if (authInstance.currentUser === null) {
//     <Link>href="/Profile" = "/Login"</Link>;
//   }

  return (
    <div className="dummyDiv">
        <h1>Dummy Game</h1>
        <h2>{counter}</h2>
        <button className='buttonDummy' onClick={increment}>Increase!</button>
        <button className='buttonDummy' onClick={decrease}>Decrease!</button>
        <button className='buttonDummy' onClick={increaseTen}>Increase By 10</button>
        <button className='buttonDummy, buttonDummySubmit' onClick={insertScore}>Submit</button>
    </div>
  );
}