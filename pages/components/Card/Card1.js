import { Card, Col, Row, Button, Text, Grid } from '@nextui-org/react';
import Link from 'next/link';

 const Card1 = () => (
  <Grid.Container gap={2}>
    {/* Card ke 1 Rock paper */}

    <Grid xs={12} sm={4}>
      <Card
        css={{
          w: '50%',
          h: '400px',
          marginLeft: '15%',
          right: '-300px',
          top: '$10',
          bottom: '$10',
        }}
      >
        <Card.Header css={{ position: 'absolute', zIndex: 1, top: 5 }}>
          <Col>
            <Text size={12} weight="bold" transform="uppercase" color="#20262E">
              Game
            </Text>
            <Text h3 color="black">
              Rock paper
            </Text>
          </Col>
        </Card.Header>
        <Card.Body css={{ p: 0 }}>
          <Card.Image
            src="https://png.pngtree.com/element_our/20200702/ourlarge/pngtree-gesture-of-rock-paper-scissors-image_2286658.jpg"
            width="100%"
            height="100%"
            objectFit="cover"
            alt="Card batu kertas"
          />
        </Card.Body>
        <Card.Footer
          isBlurred
          css={{
            position: 'absolute',
            bgBlur: '#ffffff66',
            borderTop: '$borderWeights$light solid rgba(255, 255, 255, 0.2)',
            bottom: 0,
            zIndex: 1,
          }}
        >
          <Row>
            <Col>
              <Text color="#000" size={12}>
                Batu-Gunting-Kertas adalah sebuah permainan tangan dua orang
              </Text>
            </Col>
            <Col>
              <Row justify="flex-end">
                <Link href="/RpsGame">
                  <Button flat auto rounded color="secondary">
                    <Text
                      css={{ color: 'inherit' }}
                      size={12}
                      weight="bold"
                      transform="uppercase"
                    >
                      Play
                    </Text>
                  </Button>
                </Link>
              </Row>
            </Col>
          </Row>
        </Card.Footer>
      </Card>
    </Grid>

    {/* Card Ke 2 Flappy */}

    <Grid xs={12} sm={4}>
      <Card
        css={{
          w: '50%',
          h: '400px',
          marginLeft: '127px',
          top: '6%',
          bottom: '$10',
        }}
      >
        <Card.Header css={{ position: 'absolute', zIndex: 1, top: 5 }}>
          <Col>
            <Text size={12} weight="bold" transform="uppercase" color="#20262E">
              Game
            </Text>
            <Text h3 color="black">
              Flappy Bird
            </Text>
          </Col>
        </Card.Header>
        <Card.Body css={{ p: 0 }}>
          <Card.Image
            src="https://i.pinimg.com/originals/a0/10/96/a01096406d987a54c14d498a6b420960.png"
            width="100%"
            height="100%"
            objectFit="cover"
            alt="Card Flappy"
          />
        </Card.Body>
        <Card.Footer
          isBlurred
          css={{
            position: 'absolute',
            bgBlur: '#ffffff66',
            borderTop: '$borderWeights$light solid rgba(255, 255, 255, 0.2)',
            bottom: 0,
            zIndex: 1,
          }}
        >
          <Row>
            <Col>
              <Text color="#000" size={12}>
                Flappy Bird cara bermainnya yakni dengan klik mouse
              </Text>
            </Col>
            <Col>
              <Row justify="flex-end">
                <Link href="/FlappyGame">
                  <Button flat auto rounded color="secondary">
                    <Text
                      css={{ color: 'inherit' }}
                      size={12}
                      weight="bold"
                      transform="uppercase"
                    >
                      Play
                    </Text>
                  </Button>
                </Link>
              </Row>
            </Col>
          </Row>
        </Card.Footer>
      </Card>
    </Grid>

    {/* Card ke 3 Dummy */}

    <Grid xs={12} sm={4}>
      <Card
        css={{
          w: '50%',
          h: '400px',
          marginLeft: '-125px',
          top: '6%',
          bottom: '$10',
        }}
      >
        <Card.Header css={{ position: 'absolute', zIndex: 1, top: 5 }}>
          <Col>
            <Text size={12} weight="bold" transform="uppercase" color="yellow">
              Game
            </Text>
            <Text h3 color="yellow">
              Dummy Hand
            </Text>
          </Col>
        </Card.Header>
        <Card.Body css={{ p: 0 }}>
          <Card.Image
            src="https://media.discordapp.net/attachments/1016016032088981654/1074264074969432104/SkoatS_dummy_hand_game_4k_775fe018-1e3b-42b0-a7fa-1d97adb31012.png?width=683&height=683"
            width="100%"
            height="100%"
            objectFit="cover"
            alt="backtest game"
          />
        </Card.Body>
        <Card.Footer
          isBlurred
          css={{
            position: 'absolute',
            bgBlur: '#ffffff66',
            borderTop: '$borderWeights$light solid rgba(255, 255, 255, 0.2)',
            bottom: 0,
            zIndex: 1,
          }}
        >
          <Row>
            <Col>
              <Text color="#000" size={12}>
                Backtest Game
              </Text>
            </Col>
            <Col>
              <Row justify="flex-end">
                <Link href="/DummyGames">
                  <Button flat auto rounded color="secondary">
                    <Text
                      css={{ color: 'inherit' }}
                      size={12}
                      weight="bold"
                      transform="uppercase"
                    >
                      Play
                    </Text>
                  </Button>
                </Link>
              </Row>
            </Col>
          </Row>
        </Card.Footer>
      </Card>
    </Grid>
  </Grid.Container>
);

export default Card1;
